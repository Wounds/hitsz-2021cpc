/*************************************************************************
	> File Name: convolution_forward.c
	> Author: 
	> Mail: 
	> Created Time: 
 ************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <athread.h>

#include "args.h"
#include "util.h"
#include "function.h"

#include "gargs.h"
#include "targs.h"

#include <sys/time.h>

#define lowerbound_M 256
#define lowerbound_K 256
#define lowerbound_N 256

#define upperbound_M 256
#define upperbound_K 256
#define upperbound_N 256

extern void SLAVE_FUN(gemm_rcr)(Gemm_t); //declare slave parallel method
extern void SLAVE_FUN(gemm_rrr)(Gemm_t); //declare slave parallel method
extern void SLAVE_FUN(transpose_gemm_rrr)(Gemm_t);
extern void SLAVE_FUN(trans_head)();
extern void SLAVE_FUN(trans_head_back)();

//backup
// float* local_A = (float*)aligned_malloc(sizeof(float)*upperbound_M*upperbound_K, 128);
// float* local_B = (float*)aligned_malloc(sizeof(float)*upperbound_K*upperbound_N, 128);
// float* local_C = (float*)aligned_malloc(sizeof(float)*upperbound_M*upperbound_N, 128);

static void _local_gemm_rcr(const float* A, const int LDA, const float* B, const int LDB, float* C, const int LDC, int M, int N, int K)
{
    for(int i = 0;i < M; i ++)
        for(int j = 0; j < N; j ++)
            for(int k = 0; k < K; k ++)
                C[i*LDC+j] += A[i*LDA+k]*B[k+j*LDB];
}

static void _local_gemm_rrr(const float* A, const int LDA, const float* B, const int LDB, float* C, const int LDC, int M, int N, int K)
{
    for(int i = 0;i < M; i ++)
        for(int j = 0; j < N; j ++)
            for(int k = 0; k < K; k ++)
                C[i*LDC+j] += A[i*LDA+k]*B[k*LDB+j];
}



static void _local_trans_head(float* src, float* dst, int B, int S, int D, int N)
{
    int pD = D/N;
#define SRC(b, s, d) src[b*S*D+s*D+d]
#define DST(b, n, s, pd) dst[b*N*S*pD + n*S*pD + s*pD + pd]
    for(int b = 0; b < B; b ++)
        for(int n = 0; n < N; n ++)
            for(int s = 0; s < S; s ++)
                for(int pd = 0; pd < pD; pd ++)
                    DST(b,n,s,pd) = SRC(b,s,n*pD+pd);
}

static void _local_trans_head_back(float* src, float* dst, int B, int S, int D, int N)
{
    int pD = D/N;
#define D3(b, s, d) dst[b*S*D+s*D+d]
#define D4(b, n, s, pd) src[b*N*S*pD + n*S*pD + s*pD + pd]
    for(int b = 0; b < B; b ++)
        for(int n = 0; n < N; n ++)
            for(int s = 0; s < S; s ++)
                for(int pd = 0; pd < pD; pd ++)
					D3(b,s,n*pD+pd) = D4(b,n,s,pd);
}


static void _local_norm(float* buf, int len)
{
	double sum = 0.0f;
	for(int i = 0;i < len; i ++)
		sum += buf[i];
	for(int i = 0;i < len;i ++)
		buf[i] /= sum;
}

static void _print_buf(float* buf, int len, const char* name)
{
	printf("====%s\n", name);
	for(int i = 0; i < 10 && i < len; i ++)
		printf("%f ", buf[i]);
	printf("\n");
}


static int min(int a, int b) {
	if (a < b) return a;
	return b;
}

// todo: 还需要确定矩阵首地址的位置，以及对应窗口的大小
// 因为对应窗口代表了实际运算矩阵的大小（放在local左上角位置）
// 所以实际上不需要一定填满 512 * 512，但一定要确定窗口大小和首地址

/**
 * @param gemm：待传入从核的结构体
 * @param M：待计算的矩阵的参数之一，用于表示A和C的行数
 * @param K：待计算的矩阵的参数之一，用于表示A的列数和B的行数
 * @param N：待计算的矩阵的参数之一，用于表示B和C的行数
 */
void Gemm_size(Gemm_t gemm, int M, int K, int N) {
	// int m, n, k;
	// m = lowerbound_M;
	// n = lowerbound_N;
	// k = lowerbound_K;
	
	// while (k < K)   k += lowerbound_K;
	// while (m < M)   m += lowerbound_M;
	// while (n < N)   n += lowerbound_N;

	gemm->SLAVE_K = upperbound_K;		//核组大小，单个核还需要/8
	gemm->SLAVE_M = upperbound_M;		//核组大小，单个核还需要/8
	gemm->SLAVE_N = upperbound_N;		//核组大小，单个核还需要/8
}





int getNext(int i, int m, int n)  
{  
	return (i%n)*m + i/n;  
}  
	
/* 前驱 */  
int getPre(int i, int m, int n)  
{  
	return (i%m)*n + i/m;  
}

/* 处理以下标i为起点的环 */  
void movedata(float *mtx, int i, int m, int n)  
{  
	float temp = mtx[i];  // 暂存  
	int cur = i;       // 当前下标  
	int pre = getPre(cur, m, n);  
	while(pre != i)  
	{  
		mtx[cur] = mtx[pre];  
		cur = pre; 
		pre = getPre(cur, m, n);  
	}  
	mtx[cur] = temp;  
}  
	
/* 转置，即循环处理所有环 */  
void Transpose(float *mtx, int m, int n)  
{  
	int i;
	for(i=0; i<m*n; ++i)  
	{  
		float next = getNext(i, m, n);  
		while(next > i) // 若存在后继小于i说明重复  
			next = getNext(next, m, n);  
		if(next == i)   // 处理当前环   
			movedata(mtx, i, m, n);  
	}  
}



int multihead_attention(Args_t arg)
{
	Gemm_t gemm = create_empty_gargs();


    struct timeval start, end;

    const int B = arg->B;
    const int S = arg->S;
    const int D = arg->D;
    const int N = arg->N;
    const float* x = arg->x;
    const float* w = arg->w;
    float* Q = arg->Q;
    float* K = arg->K;
    float* V = arg->V;
    float* QK = arg->QK;
    float* y = arg->y;
	const int PD = D/N;
    memset(Q, 0, sizeof(float)*B*S*D);
    memset(K, 0, sizeof(float)*B*S*D);
    memset(V, 0, sizeof(float)*B*S*D);
    memset(QK, 0, sizeof(float)*B*N*S*S);
    memset(y, 0, sizeof(float)*B*S*D);
	float* QN = (float*)aligned_malloc(sizeof(float)*B*N*S*PD, 128);
	float* KN = (float*)aligned_malloc(sizeof(float)*B*N*S*PD, 128);
	float* VN = (float*)aligned_malloc(sizeof(float)*B*N*S*PD, 128);

	int i, j, k, SLAVE_M, SLAVE_N, SLAVE_K;


    //calculate Q, K, V
	// (S*D)*(D*D)
	Gemm_size(gemm, S, D, D);
	SLAVE_M = gemm->SLAVE_M;	gemm->SLAVE_M /= 8;
	SLAVE_N = gemm->SLAVE_N;	gemm->SLAVE_N /= 8;
	SLAVE_K = gemm->SLAVE_K;	gemm->SLAVE_K /= 8;
	gemm->M = S;
	gemm->N = gemm->K = D;
	Transpose(w, D, D);
	Transpose(w+D*D, D, D);
	Transpose(w+2*D*D, D, D);
    for(int b = 0; b < B; b ++)
    {

		for (k=0; k*SLAVE_K < D; k++) {
			gemm->window_K = D - k*SLAVE_K;
			for (i=0; i*SLAVE_M < S; i++) {
				gemm->A = x+b*S*D + i*SLAVE_M * D + k*SLAVE_K;
				gemm->window_M = S - i*SLAVE_M;
				for (j=0; j*SLAVE_N < D; j++) {
					gemm->window_N = D - j*SLAVE_N;
					gemm->B = w + k*SLAVE_K*D + j*SLAVE_N;
					gemm->C = Q+b*S*D + i*SLAVE_M*D + j*SLAVE_N;
					athread_spawn(gemm_rrr, gemm);				//todo
					athread_join();

					gemm->B = w + D*D + k*SLAVE_K*D + j*SLAVE_N;
					gemm->C = K+b*S*D + i*SLAVE_M*D + j*SLAVE_N;
					athread_spawn(gemm_rrr, gemm);				//todo
					athread_join();

					gemm->B = w + 2*D*D + k*SLAVE_K*D + j*SLAVE_N;
					gemm->C = V+b*S*D + i*SLAVE_M*D + j*SLAVE_N;
					athread_spawn(gemm_rrr, gemm);				//todo
					athread_join();					
				}
			}
		}
    }

    _local_trans_head(Q, QN, B, S, D, N);
    _local_trans_head(K, KN, B, S, D, N);
    _local_trans_head(V, VN, B, S, D, N);
#define NI(b,n,s,pd) ((((b)*N+n)*S+s)*PD+pd)
#define QKI(b,n,sh,sl) ((((b)*N+n)*S+sh)*S+sl)
	// QK = Q*KT
	Gemm_size(gemm, S, PD, S);
	SLAVE_M = gemm->SLAVE_M;	gemm->SLAVE_M /= 8;
	SLAVE_N = gemm->SLAVE_N;	gemm->SLAVE_N /= 8;
	SLAVE_K = gemm->SLAVE_K;	gemm->SLAVE_K /= 8;
	gemm->M = gemm->N = S;
	gemm->K = PD;
	for(int b = 0; b < B; b ++) for(int n = 0; n < N; n ++){
		Transpose(KN+NI(b,n,0,0), S, PD);
		// _local_gemm_rcr(QN+NI(b,n,0,0), PD, KN+NI(b,n,0,0), PD, QK+QKI(b,n,0,0), S, S, S, PD);
		for (k=0; k*SLAVE_K < PD; k++) {
			gemm->window_K = PD - k*SLAVE_K;
			for (i=0; i*SLAVE_M < S; i++) {
				gemm->A = QN+NI(b, n, i*SLAVE_M, k*SLAVE_K);
				gemm->window_M = S - i*SLAVE_M;
				for (j=0; j*SLAVE_N < S; j++) {
					gemm->window_N = S - j*SLAVE_N;
					gemm->B = KN+NI(b, n, k*SLAVE_K, j*SLAVE_N);
					gemm->C = QK+QKI(b, n, i*SLAVE_M, j*SLAVE_N);
					athread_spawn(gemm_rrr, gemm);		//todo
					athread_join();		
				}
			}
		}
	}
	
	double norm = sqrt(PD*1.0);
	//normalization
	// gettimeofday(&start, NULL);
	for(int i = 0; i < B*N*S*S; i ++)
		QK[i] /= norm;									//todo
	// gettimeofday(&end, NULL);

	// Oarg QKmst;
	// QKmst.QK = QK;
	// QKmst.SUM_SIZE = B * N * S * S;
	// QKmst.norm = norm;

	for(int b = 0; b < B; b ++)
		for(int n = 0; n < N; n ++)
			for(int s = 0; s < S; s ++)
				_local_norm(QK+QKI(b,n,s,0), S);

	// reuse Q
	memset(QN, 0, sizeof(float)*B*S*D);

	// 1.需要确定从核内数据大小 SLAVE_KERNEL_{M, N, K} (4|M, 4|N)
	// 2.需要确定矩阵首地址位置
	// A = QK+QKI(b,n,0,0) + C_epoch*8*SLAVE_KERNEL_K + A_epoch*8*SLAVE_KERNEL_M*S
	// B = VN+NI(b,n,0,0) + C_epoch*8*SLAVE_KERNEL_K*D + B_epoch*8*SLAVE_KERNEL_N
	// C = QN+NI(b,n,0,0) + A_epoch*8*SLAVE_KERNEL_M*S + B_epoch*8*SLAVE_KERNEL_N
	// 3.计算
	// 4.在从核中写回C对应位置
	// (S*S) * (S*PD)
	Gemm_size(gemm, S, S, PD);
	gemm->M = gemm->K = S;
	gemm->N = PD;
	SLAVE_M = gemm->SLAVE_M;
	SLAVE_N = gemm->SLAVE_N;
	SLAVE_K = gemm->SLAVE_K;
	gemm->SLAVE_K /= 8;
	gemm->SLAVE_M /= 8;
	gemm->SLAVE_N /= 8;
	for (int b=0; b<B; b++) for (int n=0; n<N; n++) {
		// gemm->A = QK+QKI(b, n, 0, 0);
		// gemm->B = VN+NI(b, n, 0, 0);
		// gemm->C = QN+NI(b, n, 0, 0);
		// athread_spawn(gemm_rrr, gemm);
		// athread_join();								//todo 从核内自动计算分块
		for (k=0; k*SLAVE_K < S; k++) {
			gemm->window_K = S - k*SLAVE_K;
			for (i=0; i*SLAVE_M < S; i++){
				gemm->A = QK+QKI(b, n, i*SLAVE_M, k*SLAVE_K);
				gemm->window_M = S - i*SLAVE_M;
				for (j=0; j*SLAVE_N < PD; j++) {
					gemm->B = VN+NI(b, n, k*SLAVE_K, j*SLAVE_N);
					gemm->C = QN+NI(b, n, i*SLAVE_M, j*SLAVE_N);
					gemm->window_N = PD - j*SLAVE_N;
					athread_spawn(gemm_rrr, gemm);	//todo
					athread_join();
				}
			}
		}
	}


	_local_trans_head_back(QN, y, B, S, D, N);
    
	// LOG("QN: %x, size: %x", QN, sizeof(QN));
	// LOG("KN: %x, size: %x", KN, sizeof(KN));
	// LOG("VN: %x, size: %x", VN, sizeof(VN));
	aligned_free(QN);
	aligned_free(KN);
	aligned_free(VN);



	// LOG("DESTORY");
	destroy_gargs(gemm);
    return 0;
}

