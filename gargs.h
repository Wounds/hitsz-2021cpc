#ifndef _GARGS_H
#define _GARGS_H

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct Gemm
{
	float *A;
	float *B;
	float *C;
	size_t SLAVE_M;
	size_t SLAVE_N;
	size_t SLAVE_K;
	size_t window_M;
	size_t window_N;
	size_t window_K;
	size_t M;
	size_t N;
	size_t K;
}Gemm, *Gemm_t;


// typedef struct Oarg
// {
// 	float *QK;
// 	size_t SUM_SIZE;
// 	size_t norm;
// }Oarg;


#endif