#include <slave.h>
#include <math.h>
#include <simd.h>
#include <assert.h>
#include <string.h>
#include "util.h"
#include "gargs.h"

#define KERNEL_M_32 32
#define KERNEL_N_32 32
#define KERNEL_K_32 32


#define SLAVE_M 32
#define SLAVE_N 32
#define SLAVE_K 32

//向同行目标从核发送数据
#define REG_PUTR(va, row) __asm__ volatile("putr %0,%1" ::"r"(va), "r"(row))
//读行通信缓冲
#define REG_GETR(va) __asm__ volatile("getr %0" \
									  : "=r"(va))

//向同列目标从核发送数据
#define REG_PUTC(va, row) __asm__ volatile("putc %0,%1" ::"r"(va), "r"(row))
//读列通信缓存
#define REG_GETC(va) __asm__ volatile("getc %0" \
									  : "=r"(va))
#define REG_SYNR(mask) __asm__ volatile("synr %0" ::"r"(mask))
#define REG_SYNC(mask) __asm__ volatile("sync %0" ::"r"(mask))

__thread_local volatile unsigned long get_reply, put_reply;
__thread_local int id, id_row, id_col;
__thread_local Gemm local_arg;

// Must be 256bit aligned
__thread_local double local_A[KERNEL_M_32 * KERNEL_K_32] __attribute__((aligned(256)));
__thread_local double local_B[KERNEL_K_32 * KERNEL_N_32] __attribute__((aligned(256)));
__thread_local float local_C[KERNEL_M_32 * KERNEL_N_32] __attribute__((aligned(256)));

void wait_reply(volatile unsigned long *reply, int n) {
	while(*reply != n) {};
}





#define USE_ASM

typedef struct kernel_arg
{
	double *A;			  // offset 0
	double *B;			  // 8
	float *C;			  // 16
	int round_counter;	  // 24
	int row_loop_counter; // 28
	int col_loop_counter; // 32
	int id_row;			  // 36
	int id_col;			  // 40

	// Store these constant on stack because instant number must be in (0, 255)
	int A_next_col_stride; // 44
	int C_next_col_stride; // 48
} kernel_arg;

#define COMPUTE_KERNEL_32x32x32(arg) __asm__ volatile("ldl $25, 16(%0)\n\t" /* Load C_ptr first */                    \
													  "ldl $26, 0(%0)\n\t"	/* Load A_ptr */                          \
													  "ldl $27, 8(%0)\n\t"	/* Load B_ptr */                          \
													  ".START:\n\t"                                                   \
													  "vlds $0, 0($25)\n\t" /* Load 16 C, float */                    \
													  "vlds $1, 16($25)\n\t"                                          \
													  "vlds $2, 32($25)\n\t"                                          \
													  "vlds $3, 48($25)\n\t"                                          \
													  "vlds $4, 128($25)\n\t"                                         \
													  "vlds $5, 144($25)\n\t"                                         \
													  "vlds $6, 160($25)\n\t"                                         \
													  "vlds $7, 176($25)\n\t"                                         \
													  "vlds $8, 256($25)\n\t"                                         \
													  "vlds $9, 272($25)\n\t"                                         \
													  "vlds $10, 288($25)\n\t"                                        \
													  "vlds $11, 304($25)\n\t"                                        \
													  "vlds $12, 384($25)\n\t"                                        \
													  "vlds $13, 400($25)\n\t"                                        \
													  "vlds $14, 416($25)\n\t"                                        \
													  "vlds $15, 432($25)\n\t"                                        \
													  ".SELECTION:\n\t"                                               \
													  "ldw $16, 24(%0)\n\t" /* Load round_counter */                  \
													  "ldw $17, 36(%0)\n\t" /* Load id_row */                         \
													  "ldw $18, 40(%0)\n\t" /* Load id_col */                         \
													  "br $31, .COMPARE_ROW\n\t"                                      \
													  ".COMPARE_COL_1:\n\t"                                           \
													  "cmpeq $18,$16,$19\n\t"                                         \
													  "bne $19, .LOOP1\n\t"                                           \
													  "br $31, .LOOP3\n\t"                                            \
													  ".COMPARE_ROW:\n\t"                                             \
													  "cmpeq $17, $16, $19\n\t"                                       \
													  "bne $19, .COMPARE_COL_1\n\t"                                   \
													  "cmpeq $18, $16, $19\n\t"                                       \
													  "bne $19, .LOOP2\n\t"                                           \
													  ".LOOP4:\n\t"                                                   \
													  "stw $16, 24(%0)\n\t" /* Store round_counter */                 \
													  "xor $24, $24\n\t"	/* Set Counter to 0 */                    \
													  "getc $20\n\t"		/* Get column B[0] */                     \
													  "getc $21\n\t"		/* Get column B[1] */                     \
													  "getc $22\n\t"		/* Get column B[2] */                     \
													  "getc $23\n\t"		/* Get column B[3] */                     \
													  "getr $16\n\t"		/* Get row A[0] */                        \
													  ".INNER_LOOP4:\n\t"                                             \
													  "vmas $16,$20,$0,$0\n\t" /* c[0] += a[0] * b[0] */              \
													  "vmas $16,$21,$1,$1\n\t" /* c[1] += a[0] * b[1] */              \
													  "addl $24, 1, $24\n\t"                                          \
													  "vmas $16,$22,$2,$2\n\t"	 /* c[2] += a[0] * b[2] */            \
													  "getr $17\n\t"			 /* Get row A[1] */                   \
													  "vmas $16,$23,$3,$3\n\t"	 /* c[3] += a[0] * b[3] */            \
													  "getr $18\n\t"			 /* Get row A[2] */                   \
													  "vmas $17,$20,$4,$4\n\t"	 /* c[4] += a[1] * b[0] */            \
													  "getr $19\n\t"			 /* Get row A[3] */                   \
													  "vmas $18,$20,$8,$8\n\t"	 /* c[8] += a[2] * b[0] */            \
													  "getr $16\n\t"			 /* Get row A[0] */                   \
													  "vmas $19,$20,$12,$12\n\t" /* c[12] += a[3] * b[0] */           \
													  "vmas $17,$21,$5,$5\n\t"	 /* c[5] += a[1] * b[1] */            \
													  "vmas $17,$22,$6,$6\n\t"	 /* c[6] += a[1] * b[2] */            \
													  "vmas $17,$23,$7,$7\n\t"	 /* c[7] += a[1] * b[3] */            \
													  "vmas $18,$21,$9,$9\n\t"	 /* c[9] += a[2] * b[1] */            \
													  "cmpeq $24, 31, $25\n\t"                                        \
													  "vmas $19,$21,$13,$13\n\t" /* c[13] += a[3] * b[1] */           \
													  "vmas $18,$22,$10,$10\n\t" /* c[10] += a[2] * b[2] */           \
													  "getc $20\n\t"			 /* Get column B[0] */                \
													  "vmas $18,$23,$11,$11\n\t" /* c[11] += a[2] * b[3] */           \
													  "getc $21\n\t"			 /* Get column B[1] */                \
													  "vmas $19,$22,$14,$14\n\t" /* c[14] += a[3] * b[2] */           \
													  "getc $22\n\t"			 /* Get column B[2] */                \
													  "vmas $19,$23,$15,$15\n\t" /* c[14] += a[3] * b[2] */           \
													  "getc $23\n\t"			 /* Get column B[3] */                \
													  "beq $25, .INNER_LOOP4\n\t"                                     \
													  ""						 /* Tail compute */                   \
													  "vmas $16,$20,$0,$0\n\t"	 /* c[0] += a[0] * b[0] */            \
													  "getr $17\n\t"			 /* Get row A[1] */                   \
													  "vmas $16,$21,$1,$1\n\t"	 /* c[1] += a[0] * b[1] */            \
													  "getr $18\n\t"			 /* Get row A[2] */                   \
													  "vmas $16,$22,$2,$2\n\t"	 /* c[2] += a[0] * b[2] */            \
													  "getr $19\n\t"			 /* Get row A[3] */                   \
													  "vmas $16,$23,$3,$3\n\t"	 /* c[3] += a[0] * b[3] */            \
													  "ldw $25, 24(%0)\n\t"		 /* Load round_counter */             \
													  "vmas $17,$20,$4,$4\n\t"	 /* c[4] += a[1] * b[0] */            \
													  "vmas $18,$20,$8,$8\n\t"	 /* c[8] += a[2] * b[0] */            \
													  "vmas $19,$20,$12,$12\n\t" /* c[12] += a[3] * b[0] */           \
													  "vmas $17,$21,$5,$5\n\t"	 /* c[5] += a[1] * b[1] */            \
													  "addl $25,1,$25\n\t"                                            \
													  "vmas $17,$22,$6,$6\n\t"	 /* c[6] += a[1] * b[2] */            \
													  "vmas $17,$23,$7,$7\n\t"	 /* c[7] += a[1] * b[3] */            \
													  "ldl $26, 0(%0)\n\t"		 /* Reload A_ptr */                   \
													  "vmas $18,$21,$9,$9\n\t"	 /* c[9] += a[2] * b[1] */            \
													  "ldl $27, 8(%0)\n\t"		 /* Reload B_ptr */                   \
													  "vmas $19,$21,$13,$13\n\t" /* c[13] += a[3] * b[1] */           \
													  "cmpeq $25, 8, $24\n\t"                                         \
													  "vmas $18,$22,$10,$10\n\t"	/* c[10] += a[2] * b[2] */        \
													  "vmas $18,$23,$11,$11\n\t"	/* c[11] += a[2] * b[3] */        \
													  "vmas $19,$22,$14,$14\n\t"	/* c[14] += a[3] * b[2] */        \
													  "stw $25, 24(%0)\n\t"			/* Store round_counter */         \
													  "vmas $19,$23,$15,$15\n\t"	/* c[14] += a[3] * b[2] */        \
													  "bne $24, .OUTER_COMPARE\n\t" /* End 8 round */                 \
													  "br $31, .SELECTION\n\t"		/* No need to store back C */     \
													  ".OUTER_COMPARE:\n\t"                                           \
													  "ldl $25, 16(%0)\n\t" /* Load C_ptr */                          \
													  "stw $31, 24(%0)\n\t" /* Reset round_counter */                 \
													  "ldw $24, 32(%0)\n\t" /* Load col_loop_counter */               \
													  "vsts $0, 0($25)\n\t" /* Store 16 C, float */                   \
													  "vsts $1, 16($25)\n\t"                                          \
													  "vsts $2, 32($25)\n\t"                                          \
													  "vsts $3, 48($25)\n\t"                                          \
													  "vsts $4, 128($25)\n\t"                                         \
													  "addl $24, 16, $24\n\t" /* col_loop_counter += 16 */            \
													  "vsts $5, 144($25)\n\t"                                         \
													  "vsts $6, 160($25)\n\t"                                         \
													  "vsts $7, 176($25)\n\t"                                         \
													  "vsts $8, 256($25)\n\t"                                         \
													  "vsts $9, 272($25)\n\t"                                         \
													  "vsts $10, 288($25)\n\t"                                        \
													  "cmpeq $24, 32, $0\n\t"                                         \
													  "vsts $11, 304($25)\n\t"                                        \
													  "vsts $12, 384($25)\n\t"                                        \
													  "vsts $13, 400($25)\n\t"                                        \
													  "vsts $14, 416($25)\n\t"                                        \
													  "vsts $15, 432($25)\n\t"                                        \
													  "bne $0, .RESET_COL\n\t"                                        \
													  "addl $25, 64, $25\n\t"  /* C_ptr += 16 , C is float !!! */     \
													  "addl $27, 128, $27\n\t" /* B_ptr += 16 */                      \
													  "stl $25, 16(%0)\n\t"	   /* Store C_ptr */                      \
													  "stl $27, 8(%0)\n\t"	   /* Store B_ptr */                      \
													  "stw $24, 32(%0)\n\t"	   /* Store col_loop_counter */           \
													  "br $31, .START\n\t"                                            \
													  ".RESET_COL:\n\t"                                               \
													  "ldw $0, 28(%0)\n\t"	/* Load row_loop_counter */               \
													  "stw $31, 32(%0)\n\t" /* Reset col_loop_counter */              \
													  "ldw $2, 44(%0)\n\t"	/* Load A stride */                       \
													  "ldw $3, 48(%0)\n\t"	/* Load C stride */                       \
													  "addl $0, 4, $0\n\t"                                            \
													  "cmpeq $0, 32, $1\n\t"                                          \
													  "bne $1, .END\n\t"                                              \
													  "addl $26,$2,$26\n\t"	 /* move A_ptr to next row */             \
													  "stw $0, 28(%0)\n\t"	 /* Store row_loop_counter */             \
													  "subl $27,128,$27\n\t" /* move B_ptr back */                    \
													  "addl $25,$3,$25\n\t"	 /* move C_ptr to next row */             \
													  "stl $25, 16(%0)\n\t"	 /* Store C_ptr */                        \
													  "stl $26, 0(%0)\n\t"	 /* Store A_ptr */                        \
													  "stl $27, 8(%0)\n\t"	 /* Store B_ptr */                        \
													  "br $31, .START\n\t"                                            \
													  ".LOOP2:\n\t"                                                   \
													  "stw $16, 24(%0)\n\t"	 /* Store round_counter */                \
													  "xor $24, $24\n\t"	 /* Set Counter to 0 */                   \
													  "getc $20\n\t"		 /* Get column B[0] */                    \
													  "getc $21\n\t"		 /* Get column B[1] */                    \
													  "getc $22\n\t"		 /* Get column B[2] */                    \
													  "getc $23\n\t"		 /* Get column B[3] */                    \
													  "ldder $16,0($26)\n\t" /* Send row A[0] */                      \
													  ".INNER_LOOP2:\n\t"                                             \
													  "vmas $16,$20,$0,$0\n\t"	 /* c[0] += a[0] * b[0] */            \
													  "ldder $17,256($26)\n\t"	 /* Send row A[1] */                  \
													  "vmas $16,$21,$1,$1\n\t"	 /* c[1] += a[0] * b[1] */            \
													  "ldder $18,512($26)\n\t"	 /* Send row A[2] */                  \
													  "vmas $16,$22,$2,$2\n\t"	 /* c[2] += a[0] * b[2] */            \
													  "ldder $19,768($26)\n\t"	 /* Send row A[3] */                  \
													  "vmas $16,$23,$3,$3\n\t"	 /* c[3] += a[0] * b[3] */            \
													  "ldder $16,8($26)\n\t"	 /* Send row A[0] */                  \
													  "vmas $17,$20,$4,$4\n\t"	 /* c[4] += a[1] * b[0] */            \
													  "vmas $18,$20,$8,$8\n\t"	 /* c[8] += a[2] * b[0] */            \
													  "vmas $19,$20,$12,$12\n\t" /* c[12] += a[3] * b[0] */           \
													  "vmas $17,$21,$5,$5\n\t"	 /* c[5] += a[1] * b[1] */            \
													  "vmas $17,$22,$6,$6\n\t"	 /* c[6] += a[1] * b[2] */            \
													  "addl $24, 1, $24\n\t"                                          \
													  "vmas $17,$23,$7,$7\n\t" /* c[7] += a[1] * b[3] */              \
													  "vmas $18,$21,$9,$9\n\t" /* c[9] += a[2] * b[1] */              \
													  "addl $26, 8, $26\n\t"                                          \
													  "vmas $19,$21,$13,$13\n\t" /* c[13] += a[3] * b[1] */           \
													  "cmpeq $24, 31, $25\n\t"                                        \
													  "vmas $18,$22,$10,$10\n\t" /* c[10] += a[2] * b[2] */           \
													  "getc $20\n\t"			 /* Get column B[0] */                \
													  "vmas $18,$23,$11,$11\n\t" /* c[11] += a[2] * b[3] */           \
													  "getc $21\n\t"			 /* Get column B[1] */                \
													  "vmas $19,$22,$14,$14\n\t" /* c[14] += a[3] * b[2] */           \
													  "getc $22\n\t"			 /* Get column B[2] */                \
													  "vmas $19,$23,$15,$15\n\t" /* c[14] += a[3] * b[2] */           \
													  "getc $23\n\t"			 /* Get column B[3] */                \
													  "beq $25, .INNER_LOOP2\n\t"                                     \
													  ""						 /* Tail compute */                   \
													  "vmas $16,$20,$0,$0\n\t"	 /* c[0] += a[0] * b[0] */            \
													  "ldder $17,256($26)\n\t"	 /* Send row A[1] */                  \
													  "vmas $16,$21,$1,$1\n\t"	 /* c[1] += a[0] * b[1] */            \
													  "ldder $18,512($26)\n\t"	 /* Send row A[2] */                  \
													  "vmas $16,$22,$2,$2\n\t"	 /* c[2] += a[0] * b[2] */            \
													  "ldder $19,768($26)\n\t"	 /* Send row A[3] */                  \
													  "vmas $16,$23,$3,$3\n\t"	 /* c[3] += a[0] * b[3] */            \
													  "ldw $25, 24(%0)\n\t"		 /* Load round_counter */             \
													  "vmas $17,$20,$4,$4\n\t"	 /* c[4] += a[1] * b[0] */            \
													  "vmas $18,$20,$8,$8\n\t"	 /* c[8] += a[2] * b[0] */            \
													  "vmas $19,$20,$12,$12\n\t" /* c[12] += a[3] * b[0] */           \
													  "vmas $17,$21,$5,$5\n\t"	 /* c[5] += a[1] * b[1] */            \
													  "addl $25,1,$25\n\t"                                            \
													  "vmas $17,$22,$6,$6\n\t"	 /* c[6] += a[1] * b[2] */            \
													  "ldl $26, 0(%0)\n\t"		 /* Reload A_ptr */                   \
													  "vmas $17,$23,$7,$7\n\t"	 /* c[7] += a[1] * b[3] */            \
													  "ldl $27, 8(%0)\n\t"		 /* Reload B_ptr */                   \
													  "vmas $18,$21,$9,$9\n\t"	 /* c[9] += a[2] * b[1] */            \
													  "vmas $19,$21,$13,$13\n\t" /* c[13] += a[3] * b[1] */           \
													  "cmpeq $25, 8, $24\n\t"                                         \
													  "vmas $18,$22,$10,$10\n\t"	/* c[10] += a[2] * b[2] */        \
													  "vmas $18,$23,$11,$11\n\t"	/* c[11] += a[2] * b[3] */        \
													  "vmas $19,$22,$14,$14\n\t"	/* c[14] += a[3] * b[2] */        \
													  "stw $25, 24(%0)\n\t"			/* Store round_counter */         \
													  "vmas $19,$23,$15,$15\n\t"	/* c[14] += a[3] * b[2] */        \
													  "bne $24, .OUTER_COMPARE\n\t" /* End 8 round */                 \
													  "br $31, .SELECTION\n\t"		/* No need to store back C */     \
													  ".LOOP3:\n\t"                                                   \
													  "stw $16, 24(%0)\n\t"	  /* Store round_counter */               \
													  "xor $24, $24\n\t"	  /* Set Counter to 0 */                  \
													  "vldc $20, 0($27)\n\t"  /* Load and broadcast column B[0] */    \
													  "vldc $21, 32($27)\n\t" /* Load and broadcast column B[1] */    \
													  "vldc $22, 64($27)\n\t" /* Load and broadcast column B[2] */    \
													  "vldc $23, 96($27)\n\t" /* Load and broadcast column B[3] */    \
													  "getr $16\n\t"		  /* Get row A[0] */                      \
													  ".INNER_LOOP3:\n\t"                                             \
													  "vmas $16,$20,$0,$0\n\t" /* c[0] += a[0] * b[0] */              \
													  "getr $17\n\t"		   /* Get row A[1] */                     \
													  "vmas $16,$21,$1,$1\n\t" /* c[1] += a[0] * b[1] */              \
													  "getr $18\n\t"		   /* Get row A[2] */                     \
													  "vmas $16,$22,$2,$2\n\t" /* c[2] += a[0] * b[2] */              \
													  "getr $19\n\t"		   /* Get row A[3] */                     \
													  "vmas $16,$23,$3,$3\n\t" /* c[3] += a[0] * b[3] */              \
													  "getr $16\n\t"		   /* Get row A[0] */                     \
													  "vmas $17,$20,$4,$4\n\t" /* c[4] += a[1] * b[0] */              \
													  "vmas $17,$21,$5,$5\n\t" /* c[5] += a[1] * b[1] */              \
													  "addl $27, 128, $27\n\t"                                        \
													  "vmas $17,$22,$6,$6\n\t" /* c[6] += a[1] * b[2] */              \
													  "addl $27, 128, $27\n\t"                                        \
													  "vmas $17,$23,$7,$7\n\t" /* c[7] += a[1] * b[3] */              \
													  "addl $24, 1, $24\n\t"                                          \
													  "vmas $18,$20,$8,$8\n\t"	 /* c[8] += a[2] * b[0] */            \
													  "vmas $18,$21,$9,$9\n\t"	 /* c[9] += a[2] * b[1] */            \
													  "vmas $18,$22,$10,$10\n\t" /* c[10] += a[2] * b[2] */           \
													  "vmas $18,$23,$11,$11\n\t" /* c[11] += a[2] * b[3] */           \
													  "cmpeq $24, 31, $25\n\t"                                        \
													  "vmas $19,$20,$12,$12\n\t" /* c[12] += a[3] * b[0] */           \
													  "vldc $20, 0($27)\n\t"	 /* Load and broadcast column B[0] */ \
													  "vmas $19,$21,$13,$13\n\t" /* c[13] += a[3] * b[1] */           \
													  "vldc $21, 32($27)\n\t"	 /* Load and broadcast column B[1] */ \
													  "vmas $19,$22,$14,$14\n\t" /* c[14] += a[3] * b[2] */           \
													  "vldc $22, 64($27)\n\t"	 /* Load and broadcast column B[2] */ \
													  "vmas $19,$23,$15,$15\n\t" /* c[14] += a[3] * b[2] */           \
													  "vldc $23, 96($27)\n\t"	 /* Load and broadcast column B[3] */ \
													  "beq $25, .INNER_LOOP3\n\t"                                     \
													  ""						 /* Tail compute */                   \
													  "vmas $16,$20,$0,$0\n\t"	 /* c[0] += a[0] * b[0] */            \
													  "getr $17\n\t"			 /* Get row A[1] */                   \
													  "vmas $16,$21,$1,$1\n\t"	 /* c[1] += a[0] * b[1] */            \
													  "getr $18\n\t"			 /* Get row A[2] */                   \
													  "vmas $16,$22,$2,$2\n\t"	 /* c[2] += a[0] * b[2] */            \
													  "getr $19\n\t"			 /* Get row A[3] */                   \
													  "vmas $16,$23,$3,$3\n\t"	 /* c[3] += a[0] * b[3] */            \
													  "ldw $25, 24(%0)\n\t"		 /* Load round_counter */             \
													  "vmas $17,$20,$4,$4\n\t"	 /* c[4] += a[1] * b[0] */            \
													  "vmas $18,$20,$8,$8\n\t"	 /* c[8] += a[2] * b[0] */            \
													  "vmas $19,$20,$12,$12\n\t" /* c[12] += a[3] * b[0] */           \
													  "vmas $17,$21,$5,$5\n\t"	 /* c[5] += a[1] * b[1] */            \
													  "addl $25,1,$25\n\t"                                            \
													  "vmas $17,$22,$6,$6\n\t"	 /* c[6] += a[1] * b[2] */            \
													  "vmas $17,$23,$7,$7\n\t"	 /* c[7] += a[1] * b[3] */            \
													  "ldl $26, 0(%0)\n\t"		 /* Reload A_ptr */                   \
													  "vmas $18,$21,$9,$9\n\t"	 /* c[9] += a[2] * b[1] */            \
													  "ldl $27, 8(%0)\n\t"		 /* Reload B_ptr */                   \
													  "vmas $19,$21,$13,$13\n\t" /* c[13] += a[3] * b[1] */           \
													  "cmpeq $25, 8, $24\n\t"                                         \
													  "vmas $18,$22,$10,$10\n\t"	/* c[10] += a[2] * b[2] */        \
													  "vmas $18,$23,$11,$11\n\t"	/* c[11] += a[2] * b[3] */        \
													  "vmas $19,$22,$14,$14\n\t"	/* c[14] += a[3] * b[2] */        \
													  "stw $25, 24(%0)\n\t"			/* Store round_counter */         \
													  "vmas $19,$23,$15,$15\n\t"	/* c[14] += a[3] * b[2] */        \
													  "bne $24, .OUTER_COMPARE\n\t" /* End 8 round */                 \
													  "br $31, .SELECTION\n\t" /* No need to store back C */     \ 
													  ".LOOP1:\n\t"                                                   \
													  "stw $16, 24(%0)\n\t"	  /* Store round_counter */               \
													  "xor $24, $24\n\t"	  /* Set Counter to 0 */                  \
													  "vldc $20, 0($27)\n\t"  /* Load and broadcast column B[0] */    \
													  "ldder $16,0($26)\n\t"  /* Send row A[0] */                     \
													  "vldc $21, 32($27)\n\t" /* Load and broadcast column B[1] */    \
													  "vldc $22, 64($27)\n\t" /* Load and broadcast column B[2] */    \
													  "vldc $23, 96($27)\n\t" /* Load and broadcast column B[3] */    \
													  ".INNER_LOOP1:\n\t"                                             \
													  "vmas $16,$20,$0,$0\n\t" /* c[0] += a[0] * b[0] */              \
													  "ldder $17,256($26)\n\t" /* Send row A[1] */                    \
													  "vmas $16,$21,$1,$1\n\t" /* c[1] += a[0] * b[1] */              \
													  "ldder $18,512($26)\n\t" /* Send row A[2] */                    \
													  "vmas $16,$22,$2,$2\n\t" /* c[2] += a[0] * b[2] */              \
													  "ldder $19,768($26)\n\t" /* Send row A[3] */                    \
													  "vmas $16,$23,$3,$3\n\t" /* c[3] += a[0] * b[3] */              \
													  "ldder $16,8($26)\n\t"   /* Send row A[0] */                    \
													  "vmas $17,$20,$4,$4\n\t" /* c[4] += a[1] * b[0] */              \
													  "addl $27, 128, $27\n\t"                                        \
													  "vmas $17,$21,$5,$5\n\t" /* c[5] += a[1] * b[1] */              \
													  "addl $27, 128, $27\n\t"                                        \
													  "vmas $17,$22,$6,$6\n\t" /* c[6] += a[1] * b[2] */              \
													  "addl $26, 8, $26\n\t"                                          \
													  "vmas $17,$23,$7,$7\n\t" /* c[7] += a[1] * b[3] */              \
													  "addl $24, 1, $24\n\t"                                          \
													  "vmas $18,$20,$8,$8\n\t" /* c[8] += a[2] * b[0] */              \
													  "vmas $18,$21,$9,$9\n\t" /* c[9] += a[2] * b[1] */              \
													  "cmpeq $24, 31, $25\n\t"                                        \
													  "vmas $18,$22,$10,$10\n\t" /* c[10] += a[2] * b[2] */           \
													  "vmas $18,$23,$11,$11\n\t" /* c[11] += a[2] * b[3] */           \
													  "vmas $19,$20,$12,$12\n\t" /* c[12] += a[3] * b[0] */           \
													  "vldc $20, 0($27)\n\t"	 /* Load and broadcast column B[0] */ \
													  "vmas $19,$21,$13,$13\n\t" /* c[13] += a[3] * b[1] */           \
													  "vldc $21, 32($27)\n\t"	 /* Load and broadcast column B[1] */ \
													  "vmas $19,$22,$14,$14\n\t" /* c[14] += a[3] * b[2] */           \
													  "vldc $22, 64($27)\n\t"	 /* Load and broadcast column B[2] */ \
													  "vmas $19,$23,$15,$15\n\t" /* c[14] += a[3] * b[2] */           \
													  "vldc $23, 96($27)\n\t"	 /* Load and broadcast column B[3] */ \
													  "beq $25, .INNER_LOOP1\n\t"                                     \
													  ""						 /* Tail compute */                   \
													  "vmas $16,$20,$0,$0\n\t"	 /* c[0] += a[0] * b[0] */            \
													  "ldder $17,256($26)\n\t"	 /* Send row A[1] */                  \
													  "vmas $16,$21,$1,$1\n\t"	 /* c[1] += a[0] * b[1] */            \
													  "ldder $18,512($26)\n\t"	 /* Send row A[2] */                  \
													  "vmas $16,$22,$2,$2\n\t"	 /* c[2] += a[0] * b[2] */            \
													  "ldder $19,768($26)\n\t"	 /* Send row A[3] */                  \
													  "vmas $16,$23,$3,$3\n\t"	 /* c[3] += a[0] * b[3] */            \
													  "ldw $25, 24(%0)\n\t"		 /* Load round_counter */             \
													  "vmas $17,$20,$4,$4\n\t"	 /* c[4] += a[1] * b[0] */            \
													  "vmas $18,$20,$8,$8\n\t"	 /* c[8] += a[2] * b[0] */            \
													  "vmas $19,$20,$12,$12\n\t" /* c[12] += a[3] * b[0] */           \
													  "vmas $17,$21,$5,$5\n\t"	 /* c[5] += a[1] * b[1] */            \
													  "addl $25,1,$25\n\t"                                            \
													  "vmas $17,$22,$6,$6\n\t"	 /* c[6] += a[1] * b[2] */            \
													  "vmas $17,$23,$7,$7\n\t"	 /* c[7] += a[1] * b[3] */            \
													  "ldl $26, 0(%0)\n\t"		 /* Reload A_ptr */                   \
													  "vmas $18,$21,$9,$9\n\t"	 /* c[9] += a[2] * b[1] */            \
													  "ldl $27, 8(%0)\n\t"		 /* Reload B_ptr */                   \
													  "vmas $19,$21,$13,$13\n\t" /* c[13] += a[3] * b[1] */           \
													  "cmpeq $25, 8, $24\n\t"                                         \
													  "vmas $18,$22,$10,$10\n\t"	/* c[10] += a[2] * b[2] */        \
													  "vmas $18,$23,$11,$11\n\t"	/* c[11] += a[2] * b[3] */        \
													  "vmas $19,$22,$14,$14\n\t"	/* c[14] += a[3] * b[2] */        \
													  "stw $25, 24(%0)\n\t"			/* Store round_counter */         \
													  "vmas $19,$23,$15,$15\n\t"	/* c[14] += a[3] * b[2] */        \
													  "bne $24, .OUTER_COMPARE\n\t" /* End 8 round */                 \
													  "br $31, .SELECTION\n\t"		/* No need to store back C */     \
													  ".END:\n\t"                                                     \
													  "" /* END */ \ 
													  ::"r"(arg)                                                      \
													  : "$0", "$1", "$2", "$3", /* 4x4 C */                           \
														"$4", "$5", "$6", "$7",                                       \
														"$8", "$9", "$10", "$11",                                     \
														"$12", "$13", "$14", "$15",                                   \
														"$16", "$17",                                                 \
														"$18", "$19", /* 4 A */                                       \
														"$20", "$21",                                                 \
														"$22", "$23", /* 4 B */                                       \
														"$24",		  /* Loop Counter */                              \
														"$25",		  /* TMP */                                       \
														"$26",		  /* A ptr */                                     \
														"$27",		  /* B ptr */                                     \
														"cc", "memory")

void gemm_rrr(Gemm_t m_gemm)
{
	
	Gemm s_gemm;
	const int id = athread_get_id(-1);
	id_row = id >> 3;
	id_col = id - (id_row << 3);
	// if(id == 0)
	// {
	// 	printf("1\n");
	// }
	get_reply = 0;
	athread_get(PE_MODE,
				m_gemm, &s_gemm, sizeof(Gemm), 
				&get_reply, 
				0, 0, 0);
	wait_reply(&get_reply, 1);
	wait_reply(&get_reply, 1);

	// if(id == 0)
	// {
	// 	printf("2\n");
	// }
	// size_t SLAVE_M = s_gemm.SLAVE_M;
	// size_t SLAVE_N = s_gemm.SLAVE_N;
	// size_t SLAVE_K = s_gemm.SLAVE_K;
	GET(s_gemm.A, local_A, SLAVE_M, SLAVE_K, s_gemm.window_M, s_gemm.window_K, s_gemm.K);
	GET(s_gemm.B, local_B, SLAVE_K, SLAVE_N, s_gemm.window_K, s_gemm.window_N, s_gemm.N);
	GET(s_gemm.C, local_C, SLAVE_M, SLAVE_N, s_gemm.window_M, s_gemm.window_N, s_gemm.N);
	while (athread_syn(ARRAY_SCOPE, 0xffff));
	// if(id == 0)
	// {
	// 	printf("3\n");
	// }

	//----------------------------------------------ASM--------------------------------------------------------//
	floatv4 vflt;
	doublev4 vdbl;
	float *fptr = (float *)local_A;
	double *dptr = (double *)local_A;
#pragma unroll[4]
	for (int i = (KERNEL_M_32 * KERNEL_N_32 - 4); i >= 0; i -= 4)
	{
		simd_load(vflt, &fptr[i]);
		vdbl = (doublev4)vflt;
		simd_store(vdbl, &dptr[i]);
	}
#pragma unroll[4]
	fptr = (float *)local_B;
	dptr = (double *)local_B;
	for (int i = (KERNEL_K_32 * KERNEL_N_32 - 4); i >= 0; i -= 4)
	{
		simd_load(vflt, &fptr[i]);
		vdbl = (doublev4)vflt;
		simd_store(vdbl, &dptr[i]);
	}
	// No need to convert C
	// if(id == 0)
	// {
	// 	printf("4\n");
	// }
	// Compute Loop
	kernel_arg _arg = {0};
	_arg.A = local_A;
	_arg.B = local_B;
	_arg.C = local_C;
	_arg.id_row = id_row;
	_arg.id_col = id_col;
	_arg.A_next_col_stride = 128 * sizeof(double);
	_arg.C_next_col_stride = 112 * sizeof(float);
	_arg.col_loop_counter = 0;
	_arg.row_loop_counter = 0;
	COMPUTE_KERNEL_32x32x32(&_arg);


	// Store back to main memory


	PUT(local_C, s_gemm.C, SLAVE_M, SLAVE_N, s_gemm.window_M, s_gemm.window_N, s_gemm.N);
	while (athread_syn(ARRAY_SCOPE, 0xffff));
}



void GET(float* src, float* dst, int dst_row, int dst_col, int window_row, int window_col, int src_col){
	if (dst_row * id_row >= window_row || dst_col * id_col >= window_col) {
		memset(dst, 0, dst_row * dst_col * sizeof(float));
		return;	
	}
	int remain_row = window_row - dst_row * id_row;		//剩余行数
	int remain_col = window_col - dst_col * id_col;		//剩余列数
	int i, j;
	if (remain_row >= dst_row) {
		if (remain_col >= dst_col) {
			get_reply = 0;
			athread_get(PE_MODE,
						src + src_col * dst_row * id_row + dst_col * id_col,
						dst,
						dst_row*dst_col*sizeof(float), &get_reply, 0,
						(src_col - dst_col) * sizeof(float),
						dst_col * sizeof(float));
			wait_reply(&get_reply, 1);
			wait_reply(&get_reply, 1);
		} else{
			get_reply = 0;
			athread_get(PE_MODE,
						src + src_col * dst_row * id_row + dst_col * id_col,
						dst,
						dst_row*remain_col*sizeof(float), &get_reply, 0,
						(src_col - remain_col) * sizeof(float),
						remain_col * sizeof(float));
			wait_reply(&get_reply, 1);
			wait_reply(&get_reply, 1);
			for (i=dst_row-1; i>=0; i--) {
				for (j=dst_col-1; j>=remain_col; j--) {
					*(dst + i*dst_col + j) = 0;
				}
				for (j=remain_col-1; j>=0; j--) {
					*(dst + i*dst_col + j) = *(dst + i*remain_col + j);
				}
			}
		}
	} else {
		if (remain_col >= dst_col) {
			get_reply = 0;
			athread_get(PE_MODE,
						src + src_col * dst_row * id_row + dst_col * id_col,
						dst,
						remain_row*dst_col*sizeof(float), &get_reply, 0,
						(src_col - dst_col) * sizeof(float),
						dst_col * sizeof(float));
			wait_reply(&get_reply, 1);
			wait_reply(&get_reply, 1);
		} else {
			get_reply = 0;
			athread_get(PE_MODE,
						src + src_col * dst_row * id_row + dst_col * id_col,
						dst,
						remain_row * remain_col*sizeof(float), &get_reply, 0,
						(src_col - remain_col) * sizeof(float),
						remain_col * sizeof(float));
			wait_reply(&get_reply, 1);
			wait_reply(&get_reply, 1);
			for (i=dst_row-1; i>=remain_row; i--) {
				memset(dst+i*dst_col, 0, dst_col*sizeof(float));
			}
			for (i=remain_row-1; i>=0; i--) {
				for (j=dst_col-1; j>=remain_col; j--){
					*(dst + i * dst_col + j) = 0;
				}
				for (j=remain_col-1; j>=0; j--) {
					*(dst + i * dst_col + j) = *(dst + i * remain_col + j);
				}
			}
		}
	}
	return ;
}

void PUT(float* src, float* dst, int src_row, int src_col, int window_row, int window_col, int dst_col){
	if (src_row * id_row >= window_row) return;
	if (src_col * id_col >= window_col) return;
	int remain_row = window_row - src_row * id_row;
	int remain_col = window_col - src_col * id_col;
	int i, j;
	put_reply = 0;
	if (remain_row >= src_row) {
		if (remain_col >= src_col) {
			athread_put(PE_MODE,
						src,
						dst + dst_col * src_row * id_row + src_col * id_col,
						src_row*src_col*sizeof(float), &put_reply,
						(dst_col - src_col)*sizeof(float),
						src_col*sizeof(float));
		} else {
			for (i=0; i<src_row; i++) {
				for (j=0; j<remain_col; j++) {
					*(src + i*remain_col + j)  = *(src + i*src_col + j);
				}
			}
			athread_put(PE_MODE,
						src,
						dst + dst_col * src_row * id_row + src_col * id_col,
						src_row*remain_col*sizeof(float), &put_reply,
						(dst_col - remain_col)*sizeof(float),
						remain_col*sizeof(float));
		}
	} else {
		if (remain_col >= src_col) {
			athread_put(PE_MODE,
						src,
						dst + dst_col * src_row * id_row + src_col * id_col,
						remain_row*src_col*sizeof(float), &put_reply,
						(dst_col - src_col)*sizeof(float),
						src_col*sizeof(float));
		} else {
			for (i=0; i<remain_row; i++) {
				for (j=0; j<remain_col; j++) {
					*(src + i*remain_col + j) = *(src + i*src_col + j);
				}
			}
			athread_put(PE_MODE,
						src,
						dst + dst_col * src_row * id_row + src_col * id_col,
						remain_row * remain_col * sizeof(float), &put_reply,
						(dst_col - remain_col) * sizeof(float),
						remain_col * sizeof(float));
		}
	}
	wait_reply(&put_reply, 1);
	wait_reply(&put_reply, 1);
}